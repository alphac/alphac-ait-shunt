<?php

/*
Plugin Name: AlphaC AIT Language Shunt
Plugin URI: http://www.alphac.it
Description: Shunts language handling for ajax calls.
Version: 0.0.2
Author: Alessandro Morelli <development@alphac.it>
Author URI: http://www.alphac.it
License: proprietary
BitBucket Plugin URI: https://alessandromorelli@bitbucket.org/alphac/alphac-ait-shunt.git
*/

class TranslationShunt {
	protected $lpolylang;
	protected $model;
	protected $language;

	public function __construct() {
		$this->lpolylang = null;
		$this->model = null;
	}

	function get_post_translation($post) {
		$model = $this->get_model();

		if($model) {
			$transl = $model->get_translation( 'post', $post, $this->language );
		} else {
			$transl = false;
		}

		if($transl && $transl != $post) {
			return $transl;
		} else {
			return $post;
		}
	}

	protected function get_model() {
		if(is_null($this->model)) {
			$options = get_option( 'polylang' );

			if(!$options) {
				return false;
			}

			$this->model   = new PLL_Model( $options );

			if(isset($_COOKIE['pll_language'])) {
				switch($_COOKIE['pll_language']) {
					case 'it':
						$this->language = $this->model->get_language('it_IT');
						break;
					case 'en':
						$this->language = $this->model->get_language('en_US');
						break;
					default:
						$this->language = $this->model->get_language(get_locale());
						break;
				}
			} else {
				$this->language = $this->model->get_language(get_locale());
			}
		}

		return $this->model;
	}

	// Utility hooks
	public function redefine_locale($locale) {
		if(isset($_COOKIE['pll_language'])) {
			switch($_COOKIE['pll_language']) {
				case 'en':
					$locale = 'en_US';
					break;
				case 'it':
					$locale = 'it_IT';
					break;
			}
		}

		return $locale;
	}

	public function catch_translated_post($room) {
		$orig = $room->resid;

		$model = $this->get_model();

		$transl = $model->get_translation('post', $orig, $this->language);

		if($transl && $transl != $orig) {
			$post               = get_post( $transl );
			$room->resid        = $transl;
			$room->post_content = $post->post_content;
			$room->post_title   = $post->post_title;
		}

		return $room;
	}
}

$shunt = new TranslationShunt();
add_filter('easy_search_resources', array($shunt, 'catch_translated_post'), 1000 ,1);
add_filter('locale', array($shunt, 'redefine_locale'));
